# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.5

- patch: Fix the COMMAND parameter validation

## 0.2.4

- patch: Update default S3_BUCKET name in readme.

## 0.2.3

- patch: Made wait interval configurable

## 0.2.2

- patch: Standardising README and pipes.yml.

## 0.2.1

- patch: Fixed incorrect reference to 'parameters' instead of 'variables' in the YAML definition.

## 0.2.0

- minor: Add support for the DEBUG flag.
- minor: Switch naming convention from tasks to pipes.

## 0.1.2

- minor: Use quotes for all pipes examples in README.md.

## 0.1.1

- minor: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines AWS Elastic Beanstalk deployment pipe.

